Name:		perl-ExtUtils-Config
Version:	0.009
Release:        2
Summary:	A wrapper for perl's configuration
License:	GPL-1.0-or-later OR Artistic-1.0-Perl
URL:		https://metacpan.org/release/ExtUtils-Config
Source0:	https://cpan.metacpan.org/modules/by-module/ExtUtils/ExtUtils-Config-%{version}.tar.gz
BuildArch:	noarch
BuildRequires:	coreutils findutils make perl-generators perl-interpreter
BuildRequires:	perl(ExtUtils::MakeMaker)

%description
This package contains the module that provides a wrapper for perl's configuration.

%package_help

%prep
%autosetup -n ExtUtils-Config-%{version} -p1

%build
perl Makefile.PL INSTALLDIRS=vendor  NO_PACKLIST=1 
%make_build

%install
make pure_install DESTDIR=%{buildroot}
%{_fixperms} -c %{buildroot}

%check
make test

%files
%license LICENSE
%{perl_vendorlib}/ExtUtils/

%files help
%doc Changes README
%{_mandir}/man3/ExtUtils::Config.3*
%{_mandir}/man3/ExtUtils::Config::MakeMaker.3*

%changelog
* Sat Jan 18 2025 Funda Wang <fundawang@yeah.net> - 0.009-2
- drop useless perl(:MODULE_COMPAT) requirement

* Fri Jun 7 2024 openEuler guoshengsheng <guoshengsheng@kylinos.cn> - 0.009-1
- update to version 0.009
- Add a but method
- Add ExtUtils::Config::MakeMaker


* Sat Feb 29 2020 openEuler Buildteam <buildteam@openeuler.org> - 0.008-19
- Package init
